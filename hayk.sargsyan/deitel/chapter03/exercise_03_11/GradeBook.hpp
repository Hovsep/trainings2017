#include <string>

class GradeBook
{
public:
    GradeBook(std::string name, std::string instructorName);
    void setCourseName(std::string name);
    std::string getCourseName();
    void displayMessage();
    void setInstructorName(std::string instructorName);
    std::string getInstructorName();
private:
    std::string courseName_;
    std::string instructorName_;
};
