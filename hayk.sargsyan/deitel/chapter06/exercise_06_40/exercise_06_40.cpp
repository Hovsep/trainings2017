#include <iostream>
#include <assert.h>

int
power(const int base, const int exponent) 
{
    assert(exponent > -1);

    if (0 == exponent) {
        return 1;
    } else {
        return base * power(base, exponent - 1);
    }
}

int
main()
{
    std::cout << power(3, 4) << std::endl;

    return 0;
}
