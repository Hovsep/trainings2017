#include <iostream>
#include <algorithm>

float
minimum(const float number1, const float number2, const float number3) 
{
    float minimum = std::min(number1, number2);
    minimum = std::min(minimum, number3);

    return minimum;
}

int
main()
{
    float number1;
    float number2;
    float number3;
    
    std::cout << "Enter a float number with double precision : ";
    std::cin >> number1 >> number2 >> number3;
    std::cout << "The minimum of numbers: "<< minimum(number1, number2, number3) << std::endl;

    return 0;
}
