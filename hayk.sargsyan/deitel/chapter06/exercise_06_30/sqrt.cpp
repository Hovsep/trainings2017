#include <iostream>
#include <cmath>

bool 
simple(const int number)
{
    if (1 == number) {
        return false;
    }
    if (2 == number) {
        return true;
    }
    for (int counter = std::sqrt(number); counter > 1; --counter) {
        if (0 == number % counter) {
            return false;
        }
    }
    return true;
}

int
main()
{
    if (simple(2)) {
        std::cout << "2 is simple" << std::endl; 
    }
    for (int counter = 1; counter <= 10000; counter += 2) {
        if (simple(counter)) {
            std::cout << counter << " is simple" << std::endl; 
        }
    }

    return 0;
}
