#include <iostream>

int
main()
{
    {///a
        int x;

        std::cout << "Enter a number (x): ";
        std::cin >> x;

        int y;

        std::cout << "Enter a number (y): ";
        std::cin >> y;

        if ((x >= 5 && y < 7) == (!(x < 5) && !(y >= 7))) {
            std::cout << "First Expression is true." << std::endl;
        }
    }

    {///b
        int a;

        std::cout << "Enter a number (a): ";
        std::cin >> a;

        int b;

        std::cout << "Enter a number (b): ";
        std::cin >> b;
        
        int g;

        std::cout << "Enter a number (g): ";
        std::cin >> g;

        if ((a != b || 5 == g) == (!(a == b) || !(g != 5))) {
            std::cout << "Second Expression is true." << std::endl; 
        }
    }

    {///c
        int x;

        std::cout << "Enter a number (x): ";
        std::cin >> x;

        int y;

        std::cout << "Enter a number (y): ";
        std::cin >> y;

        if ((x > 8 || y <= 4) == (!((x <=8) && (y > 4)))) {
            std::cout << "Third Expression is true." << std::endl;  
        }
    }

    {///d
        int i;

        std::cout << "Enter a number (i): ";
        std::cin >> i;

        int j;

        std::cout << "Enter a number (j): ";
        std::cin >> j;

        if ((i <= 4 && j > 6) == (!((i > 4) || (j <= 6)))) {
            std::cout << "Forth Expression is true." << std::endl; 
        }
    }
}
