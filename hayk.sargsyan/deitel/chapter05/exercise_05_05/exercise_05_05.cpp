#include <iostream>

int
main()
{
    int quantity;

    std::cout << "Enter the quantity of numbers: ";
    std::cin >> quantity;

    if (quantity < 1) {
        std::cerr << "Error 1: Quantity can't be zero or negative." << std::endl;

        return 1;
    }

    int sum = 0;

    for (int counter = 1; counter <= quantity; ++counter) {
        int number;
        
        std::cout << "Enter number (" << counter << " of " << quantity << ")";
        std::cin >> number;

        sum += number;
    }

    std::cout << "The sum of entered numbers: " << sum << std::endl;

    return 0;
}
