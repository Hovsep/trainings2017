#include "GradeBook.hpp"

#include <iostream>
#include <string>

GradeBook::GradeBook(std::string nameOfCourse, std::string nameOfInstructor)
{
    setCourseName(nameOfCourse);
    setInstructorName(nameOfInstructor);
}

void 
GradeBook::setCourseName(std::string nameOfCourse)
{
    courseName_ = nameOfCourse;
}

std::string 
GradeBook::getCourseName()
{
    return courseName_;
}

void 
GradeBook::setInstructorName(std::string nameOfInstructor)
{
    instructorName_ = nameOfInstructor;
}

std::string 
GradeBook::getInstructorName()
{
    return instructorName_;
}

void 
GradeBook::displayMessage()
{
    std::cout << "Welcome to the grade book for\n" << getCourseName() << "!\nThis course is presented by: " << getInstructorName() << std::endl;
}

