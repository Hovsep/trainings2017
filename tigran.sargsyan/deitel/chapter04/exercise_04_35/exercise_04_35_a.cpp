#include <iostream>

int
main()
{
    int number;
    std::cout << "Enter number: ";
    std::cin >> number;

    if (number < 0) {
        std::cerr << "Error 1. Entered number should be non-negative." << std::endl;
        return 1;
    }

    int factorial = 1;
    while (number != 0) {
        factorial *= number;
        --number;
    }

    std::cout << "Factorial: " << factorial << std::endl;

    return 0;
}

