#include <iostream>
#include <climits>

int
main()
{
    int largest = INT_MIN, counter = 1;
    
    while (counter <= 10) {
        int number;
        std::cout << "Enter number: ";
        std::cin >> number;

        if (largest < number) {
            largest = number;
        }

        counter++;
    }
    
    std::cout << "Largest number is: " << largest << std::endl;
    
    return 0;
}

