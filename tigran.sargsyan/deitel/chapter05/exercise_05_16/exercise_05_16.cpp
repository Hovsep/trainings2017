#include <iostream>
#include <iomanip>

int
main()
{
    std::cout << "Year" << std::setw(21) << "Amount of deposit" << std::endl;
    std::cout << std::fixed << std::setprecision(2);

    int amount = 100000, rate = 5;
    int accumulation = 100 + rate;
    for (int year = 1; year <= 10; ++year) {
        amount = amount * accumulation / 100;
        int amountDollars = amount / 100;
        int amountCents = amount % 100;
        std::cout << std::setw(4) << year << std::setw(18) << amountDollars
            << "." << amountCents << std::endl;
    }

    return 0;
}
