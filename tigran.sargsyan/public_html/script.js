$(document).ready(function() {
    $('.border').on('click', function() {
        $(this).parent().parent().toggle("fold", 3000).promise().done(function(){
            $(this).replaceWith();
            $('.slider').css("display", "block");
        });
        
        var slideIndex = 0;
        showSlides(slideIndex);
                
        function currentSlide(n) {
            slideIndex = n;
            showSlides(slideIndex);
        }
        
        function showSlides(n) {
            var i;
            var slides = $('.mySlides');
            var dots = $('.dot');
            if (n > slides.length) {
                slideIndex = 1;
            }
            if (n < 1) {
                slideIndex = slides.length;
            }
            for (i = 0; i < slides.length; ++i) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < dots.length; ++i) {
                dots[i].className = dots[i].className.replace(" active","");
            }
            slides[slideIndex - 1].style.display = "block";
            dots[slideIndex - 1].className += " active";
        }
        
        $('.dot').on('click', function() {
            currentSlide(this.id);
        });
        $('.prev').on('click', function() {
            showSlides(--slideIndex);
        });
        $('.next').on('click', function() {
            showSlides(++slideIndex);
        });

        var timeId = setInterval(autoSlide, 10000);
        autoSlide();
        function autoSlide() {
            var i;
            var slides = $('.mySlides');
            var dots = $('.dot');
            for (i = 0; i < slides.length; ++i) {
                slides[i].style.display = "none";
            }
            ++slideIndex;
            if (slideIndex > slides.length) {
                slideIndex = 1;
            }
            for (i = 0; i < dots.length; ++i) {
                dots[i].className = dots[i].className.replace(" active","");
            }
            slides[slideIndex - 1].style.display = "block";
            dots[slideIndex - 1].className += " active";
        }
        
        $('.prev, .next, .dot').on('mouseover', function() {
            clearTimeout(timeId);
        }).on('mouseout', function() {
            timeId = setInterval(autoSlide, 10000);
        });
    });
});
