#include "Date.hpp"
#include <iostream>

int
main()
{  
    int month = 1;
    int day = 1;
    int year = 1;

    Date date(month, day, year);
    
    std::cout << "Enter the month: ";
    std::cin  >> month;
    date.setMonth(month);

    std::cout << "Enter the day: ";
    std::cin  >> day;
    date.setDay(day);
    
    std::cout << "Enter the year: ";
    std::cin  >> year;
    date.setYear(year);

    std::cout << "Date -> ";
    date.displayDate();

    return 0;
}
