#include <string>

class Employee
{
public:
     Employee(std::string employeeName, std::string employeeSurname, int employeeSalary);
     void setEmployeeName(std::string employeeName);
     void setEmployeeSurname(std::string employeeSurname);
     void setEmployeeSalary(int employeeSalary);
     std::string getEmployeeName();
     std::string getEmployeeSurname();
     int getEmployeeSalary();
     
private:
    std::string employeeName_;
    std::string employeeSurname_;
    int employeeSalary_;
};
