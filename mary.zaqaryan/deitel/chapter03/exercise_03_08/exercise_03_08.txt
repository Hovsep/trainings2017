The header file is .hpp (.h) file. It includes information about class, class's data members and functions.
The source-code files is .cpp file, which includes a class definition and body of member functions of class. We can include header file in className.cpp with #include "header.hpp".
