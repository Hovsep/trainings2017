#include <iostream>

int
main()
{
    int accurancy;
    std::cout << "Enter a accurancy for computing number e with degree." << std::endl;
    std::cin >> accurancy;
    
    int xDegree;
    std::cout << "Enter a degrre for e: ";
    std::cin >> xDegree;

    double factorial = 1;
    double eNumber = 1;
    int counter = 1;
    int numerator = 1;
    
    while (accurancy >= counter) {
        factorial *= counter;
        numerator *= xDegree;
        eNumber += numerator / factorial;
        ++counter;
    }

    std::cout << "e number is: " << eNumber << std::endl;
    return 0;
}
