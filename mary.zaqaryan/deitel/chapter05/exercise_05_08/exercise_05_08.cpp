#include <iostream>
#include <climits>

int 
main()
{
    int amount;
    std::cout << "Enter the amount of numbers.";
    std::cin >> amount;

    if (amount < 0) {
        std::cerr << "Error 1: amount can't be negative or equal 0.";
        return 1;
    }

    int min = INT_MAX;
    
    for (int i = 1; i <= amount; ++i) {
        int number;
        std::cout << "Enter number.";
        std::cin >> number;
        if (number < min) {
            min = number;
        }
    }

    std::cout << "Minimum is " << min << std::endl;

    return 0;
}
