#include <iostream>

int
main() 
{
    int sum = 0;
    int counter = 0;

    while (true) {
        int number;
        std::cout << "Enter the number (9999 to quit): ";
        std::cin >> number;

        if (9999 == number) {
            break;
        }

        sum += number;
        ++counter;
    }

    double sequence = static_cast<double>(sum);
    double average = sequence / counter;
    std::cout << "Average is: " << average << std::endl;
    return 0;
}
