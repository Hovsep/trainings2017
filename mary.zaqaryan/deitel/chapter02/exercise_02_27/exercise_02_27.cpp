#include <iostream>

int 
main()
{
    char symbol;
    std::cout << "Enter a symbol: ";
    std::cin >> symbol;
    std::cout << "The ASCII number for this symbol is: " << static_cast<int>(symbol) << "\n";
    return 0;
}

