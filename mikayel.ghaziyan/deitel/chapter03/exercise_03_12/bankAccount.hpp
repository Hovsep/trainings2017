#include <string> 


class Account
{
public:
    Account(int initialBalance);
    void setBankAccount(int initialBalance);
    void credit(int deposit);
    void debit(int withdraw);
    int  getBalance();  

private:
    int accountBalance_;
};

