/// Mikayel Ghaziyan
/// 15/10/2017
/// Exercise 2.18

#include <iostream>

int   /// Beginning of the main function
main()
{
    /// Declaring variables
    int number1;
    int number2;

    /// Obtaining numbers from the user
    std::cout << "Please enter the first number: ";
    std::cin >> number1;
    std::cout << "Please enter the second number: ";
    std::cin >> number2;

    /// Evaluating the numbers
    if (number1 > number2) {
        std::cout << number1 << " is lareger than " << number2 << "\n";

	return 0; 
    }
    if (number2 > number1) {
        std::cout << number2 << " is lareger than " << number1 << "\n";

	return 0;
    }
    
     std::cout << "The numbers are equal\n";
   
    return 0;
} /// End of the main function

/// End of the file

