#include <iostream>
#include <cmath>
#include <cassert>

void
drawSolidRectangle(const int side, const char fillCharacter) 
{
    assert(side > 0);            
    for (int count1 = 1; count1 <= side; ++count1) {
        for (int count2 = 1; count2 <= side; ++count2) {
            std::cout << fillCharacter;
        }

        std::cout << std::endl;
    }
}

void
drawSpacedRectangle(const int side, const char fillCharacter) 
{
    assert(side > 0);            
    for (int count1 = 1; count1 <= side; ++count1) {
        for (int count2 = 1; count2 <= side; ++count2) {
            if (1 == count2 || 1 == count1 || side == count1 || side == count2) {
                std::cout << fillCharacter;
            } else {
                std::cout << " ";
            }
        }

        std::cout << std::endl;
    }
}

void
drawTriangle(const int side, const char fillCharacter) 
{
    assert(side > 0);            
    for (int count1 = 1; count1 <= side; ++count1) {
        for (int count2 = 1; count2 <= side; ++count2) {
            if (count2 <= count1) {
                std::cout << fillCharacter;
            }
        }

        std::cout << std::endl;
    }
}

void
drawRhombus(const int side, const char fillCharacter)
{
    assert(side > 0);
    for (int count1 = -side / 2; count1 <= side / 2; ++count1) {
        for (int count2 = -side / 2; count2 <= side / 2; ++count2) {
            std::cout << ((std::abs(count2) + std::abs(count1) <= side / 2) ? fillCharacter : ' '); 
        }

        std::cout << std::endl;
    }
}

void
draw(const int side, const char fillCharacter, const int shape) 
{
    assert(side > 0);
    assert(shape > 0 && shape < 5);

    if (1 == shape) {
        drawtriangle(side, fillCharacter);
    } else if (2 == shape) {
        drawsolidRectangle(side, fillCharacter); 
    } else if (3 == shape) {
        drawrhombus(side, fillCharacter);
    } else if (4 == shape) {
        drawspacedRectangle(side, fillCharacter);
    }
}

int
main()
{
    int side;
    std::cout << "Enter the side: ";
    std::cin >> side;

    if (side < 1) {
        std::cerr << "Error 1: Non positive side." << std::endl;
        return 1;
    }

    char fillCharacter;
    std::cout << "Enter the character: ";
    std::cin >> fillCharacter;

    int shape;
    std::cout << "Choose number of shape: " << std::endl
        << "1 - triangle" << std::endl
        << "2 - solid rectangle" << std::endl
        << "3 - rhombus" << std::endl
        << "4 - spaced rectangle" << std::endl;
    std::cin >> shape;

    if (shape < 1 || shape > 4) {
        std::cerr << "Error 2: There is no such shape." << std::endl;
        return 2;
    }

    draw(side, fillCharacter, shape);

    return 0;
}

