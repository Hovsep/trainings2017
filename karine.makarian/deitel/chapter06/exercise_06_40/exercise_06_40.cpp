#include <iostream>
#include <cassert>

int power(const int base, const int exponent);

int
main()
{
    int base, exponent;
    std::cout << "Enter base and exponent.";
    std::cin >> base >> exponent;

    const int result = power(base, exponent);
    std::cout << "The power is " << result << std::endl;

    return 0;
}

int
power(const int base, const int exponent)
{
    assert(exponent > 0);
    if(1 == exponent) {
        return base;
    }

    return base * power(base, exponent - 1);
}
