#include <iostream>

double
minimum(const double number1, const double number2, const double number3) 
{
    const double minimum = (number1 > number2) ? number2 : number1;
    return (minimum > number3) ? number3 : minimum;
}

int
main()
{
    double number1;
    double number2;
    double number3;

    std::cout << "Enter 3 float numbers with double precision : ";
    std::cin >> number1 >> number2 >> number3;
    std::cout << "The minimum of numbers is " << minimum(number1, number2, number3) << std::endl;

    return 0;
}

