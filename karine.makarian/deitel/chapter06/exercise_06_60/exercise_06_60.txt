a) template < class A >
int sum( int num1, int num2, int num3 )
{
    return num1 + num2 + num3; ///in all places written "int" must be "A"
}

b) 
    void printResults( int x, int у )
    {
        cout « "The sum is " « x + у « ' \n';
        return x + y; /// can't return smth, because function's return type is void
    }
c)
    template < A > ///must be < typename A >
    A product ( A numl, A num2, A num3 )
    {
        return numl * num2 * num3;
    }

d)
    double cube( int );
    int cube( int ) ; ///Compiler will give an error because it  has no way to determine which version to call 
                            ///(2nd prototype ambiguates the decleration of the 1st).
                      /// Need to use function overloading, which gives an opportunity to have functions having the same name 
                            ///but different argument types.
