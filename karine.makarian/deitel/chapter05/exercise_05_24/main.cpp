#include <iostream>
#include <cmath>

int 
main()
{
    int row;
    
    std::cout << "Enter the odd count of rows (from 1 to 19): ";
    std::cin >> row;

    if(row % 2 == 0) {
        std::cerr << "Error 1: Even number was inputed." << std::endl;
        return 1;
    }
    
    std::cout << std::endl;
    row /= 2;

    for (int i = -row; i <= row; ++i) {       
        for (int j = -row; j <= row; ++j) {
            std::cout << ((std::abs(i) + std::abs(j) <= row) ? "*" : " ");      
        }
        std::cout << std::endl;
    }
    return 0;
}

