#include <iostream>

int
main()
{
    {///a
        int x;
    
        std::cout << "Enter the first number: ";
        std::cin >> x;

        int y;

        std::cout << "Enter the second number: ";
        std::cin >> y;

        if ((x >= 5 && y < 7) == (!(x < 5) && !(y >= 7))) {
            std::cout << "The first expression is true." << std::endl;
        }
    }

    {///b
        int x;
    
        std::cout << "Enter the first number: ";
        std::cin >> x;
    
        int y;

        std::cout << "Enter the second number: ";
        std::cin >> y;

        int z;

        std::cout << "Enter the third number: ";
        std::cin >> z;
    
        if ((x != y || 5 == z) == (!(x == y) || !(z != 5))) {
            std::cout << "The second expression is true." << std::endl; 
        }
    }

    {///c
        int x;
    
        std::cout << "Enter the first number: ";
        std::cin >> x;

        int y;
    
        std::cout << "Enter the second number: ";
        std::cin >> y;

        if ((x > 8 || y <= 4) == (!((x <=8) && (y > 4)))) {
            std::cout << "The third expression is true." << std::endl;  
        }
    }

    {///d
        int x;
    
        std::cout << "Enter the first number: ";
        std::cin >> x;

        int y;

        std::cout << "Enter the second number: ";
        std::cin >> y;
    
        if ((x <= 4 && y > 6) == (!((x > 4) || (y <= 6)))) {
            std::cout << "The forth expression is true." << std::endl; 
        }
    }
}

