#include <iostream>
#include "GradeBook.hpp"

int
main()
{
    GradeBook myGradeBook("Programming C++");
    myGradeBook.displayMessage();
    myGradeBook.inputGrades();
    myGradeBook.displayGradeReport();
    myGradeBook.displayAverage();

    return 0;
}

