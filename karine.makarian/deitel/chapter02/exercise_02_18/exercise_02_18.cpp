#include <iostream>

int
main()
{
    int number1;
    int number2;

    std::cout << "Enter first number: ";
    std::cin >> number1;

    std::cout << "Enter second number: ";
    std::cin >> number2;

    if (number1 > number2) {
        std::cout << number1 << "is larger\n";
        return 1;
    }
  
    if (number1 < number2) {
	std::cout << number2 << "is larger\n";
        return 1;
    }
   
    std::cout << "This numbers are equal\n";
    return 0;
}

