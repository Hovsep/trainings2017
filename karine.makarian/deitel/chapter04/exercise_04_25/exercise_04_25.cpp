#include <iostream>

int 
main()
{
    int size;

    std::cout << "Enter the size of the side of the square: ";
    std::cin >> size;

    if (size < 1) {
        std::cerr << "Error 1: Invalid size." << std::endl;
        return 1;
    }
    
    if (size > 20) {
        std::cerr << "Error 1: Invalid size." << std::endl;
        return 1;
    }
    
    std::cout << std::endl;

    int count1 = 1;

    while (count1 <= size) {
        int count2 = 1;
        while (count2 <= size) {
            if (1 == count1) {
                std::cout << "*";
            } else if (size == count1) {
                std::cout << "*"; 
            } else if (1 == count2) {
                std::cout << "*";
            } else if (size == count2) {
                std::cout << "*";
            } else {
                std::cout << " "; 
            }

            ++count2;
        }
    
        std::cout << std::endl;

        ++count1;     
    }
    
    return 0;
}

