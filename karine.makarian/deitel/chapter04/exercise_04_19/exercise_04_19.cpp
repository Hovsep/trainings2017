#include <iostream>
#include <climits>

int
main()
{ 
    int counter = 1;
    int number;

    std::cout << "Enter 10 numbers: ";
    int largest = INT_MIN;
    int secondLargest = INT_MIN;
    
    while (counter <= 10) {
        std::cin >> number;

        if (number > largest) {
            secondLargest = largest;
            largest = number;
        } else if(number > secondLargest) {
            secondLargest = number;
        }

        ++counter;
    }

    std::cout << "The largest value is: " << largest << std::endl;
    std::cout << "The second largest value is: " << secondLargest << std::endl;

    return 0;
}

