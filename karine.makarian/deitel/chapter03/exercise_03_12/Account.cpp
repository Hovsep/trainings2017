#include "Account.hpp"
#include <iostream>

Account::Account(int initialBalance)
{
    setBalance(initialBalance);
}

void
Account::setBalance(int initialBalance)
{
    if(initialBalance >= 0) {
        balance_ = initialBalance;
        return;
    }
    balance_ = 0;
    std::cout << "Error 1: Invalid initial balance." << std::endl;
}

int 
Account::getBalance()
{
    return balance_;
}

int 
Account::credit(int amount)
{
    balance_ = balance_ + amount;
    return balance_;
}

int 
Account::debit(int amount)
{
    if (amount > balance_) {
        std::cout << "Error 2: Amount exceeded account balance." << std::endl;
        return balance_;
    }
    balance_ = balance_ - amount; 
    return balance_;
}

