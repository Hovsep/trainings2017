#include "Employee.hpp"
#include <iostream>
#include <string>

int
main()
{
    std::string firstName1 = "Hayk";
    std::string firstName2 = "Vardan";
    std::string lastName1 = "Hayrapetyan";
    std::string lastName2 = "Grigoryan";
    int salary1 = 100000;
    int salary2 = 230000;
    int count = 12;

    Employee employee1(firstName1, lastName1, salary1);
    Employee employee2(firstName2, lastName2, salary2);

    std::cout << "employee1's yearly salary is " << count * employee1.getMonthSalary() << std::endl;
    std::cout << "employee2's yearly salary is " << count * employee2.getMonthSalary() << std::endl;
    salary1 = 120000;
    salary2 = 250000;
    employee1.setMonthSalary(salary1);
    employee2.setMonthSalary(salary2);
    
    std::cout << "employee1's new monthly salary is " << employee1.getMonthSalary() <<std::endl;
    std::cout << "employee2's new monthly salary is " << employee2.getMonthSalary() <<std::endl;
   
    employee1.setMonthSalary(employee1.getMonthSalary() / 10 + employee1.getMonthSalary());
    employee2.setMonthSalary(employee2.getMonthSalary() / 10 + employee2.getMonthSalary());

    std::cout << "After 10% raise employee1's yearly salary is " << count * employee1.getMonthSalary() << std::endl;
    std::cout << "After 10% raise employee2's yearly salary is " << count * employee2.getMonthSalary() << std::endl;
    return 0;
}

