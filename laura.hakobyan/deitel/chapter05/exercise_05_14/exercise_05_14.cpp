#include <iostream>

int
main()
{
    double totalRetailValue = 0.00;
    while (true) {
        int productNumber;
        std::cout << "Enter number of product: ";
        std::cin >> productNumber;
        if (-1 == productNumber) {
            break;
        }

        int quantitySold;
        std::cout << "Enter quantity: ";
        std::cin >> quantitySold;
        if (quantitySold <= 0) {
            std::cout << "Error 1: Quantity of product cannot be negative." <<std::endl;
            return 1;
        }
        
        switch (productNumber) {
        case 1: totalRetailValue += quantitySold * 2.98; break;
        case 2: totalRetailValue += quantitySold * 4.50; break;
        case 3: totalRetailValue += quantitySold * 9.98; break;
        case 4: totalRetailValue += quantitySold * 4.49; break;
        case 5: totalRetailValue += quantitySold * 6.87; break;
        default:
            std::cout << "Error 2: There isn't product with that number." << std::endl;
            break;
        }
    }
    std::cout << "The total retail value is " << totalRetailValue << "$" << std::endl;
    return 0;
}

