#include <iostream>

int
main()
{
    for (int couplet = 1; couplet <= 12; ++couplet) {
        switch (couplet) {
        case  1: std::cout << "On the first day of Christmas\n"    ; break; 
        case  2: std::cout << "On the second day of Christmas\n"   ; break; 
        case  3: std::cout << "On the third day of Christmas\n"    ; break; 
        case  4: std::cout << "On the fourth day of Christmas\n"   ; break; 
        case  5: std::cout << "On the fifth day of Christmas\n"    ; break; 
        case  6: std::cout << "On the sixth day of Christmas\n"    ; break; 
        case  7: std::cout << "On the seventh day of Christmas\n"  ; break; 
        case  8: std::cout << "On the eighth day of Christmas\n"   ; break; 
        case  9: std::cout << "On the ninth day of Christmas\n"    ; break; 
        case 10: std::cout << "On the tenth day of Christmas\n"    ; break; 
        case 11: std::cout << "On the eleventh day of Christmas\n" ; break; 
        case 12: std::cout << "On the twelfth day of Christmas \n" ; break; 
        }

        std::cout << "my true love sent to me:\n";
        switch (couplet) {
        case 12: std::cout << "12 Drummers Drumming\n"        ; 
        case 11: std::cout << "Eleven Pipers Piping\n"        ; 
        case 10: std::cout << "Ten Lords a Leaping\n"         ; 
        case  9: std::cout << "Nine Ladies Dancing\n"         ; 
        case  8: std::cout << "Eight Maids a Milking\n"       ; 
        case  7: std::cout << "Seven Swans a Swimming\n"      ; 
        case  6: std::cout << "Six Geese a Laying\n"          ; 
        case  5: std::cout << "Five Golden Rings\n"           ; 
        case  4: std::cout << "Four Calling Birds\n"          ; 
        case  3: std::cout << "Three French Hens\n"           ; 
        case  2: std::cout << "Two Turtle Doves\n"            ;    break;
        case  1: std::cout << "A Partridge in a Pear Tree\n\n"; continue;
        }
        std::cout << "and a Partridge in a Pear Tree\n" << std::endl;
    }

    return 0;
}
