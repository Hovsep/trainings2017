#include <iostream>
#include <iomanip>

int
main()
{
    std::cout << std::fixed << std::setprecision(2);
    for (int integerRate = 5; integerRate <= 10; ++integerRate) {
        double rate = integerRate * 0.01;
        long double amount = 24.0;
        std::cout << "==================================================================================\n"
                  << "---------------------Peter Minuit Deposite by the years---------------------------\n"
                  << "==================================================================================\n"
                  << "------------------------This table for " << std::setw(2) << integerRate << "%" 
                  << " per annum -----------------------------\n"
                  << "__________________________________________________________________________________\n";

        std::cout << "Year" << std::setw(25) << "Amount on deposit" << std::endl;
        for (int year = 1626; year <= 2018; ++year) {
            amount *= (1.0 + rate);
            std::cout << std::setw(4) << year << std::setw(25) << amount << std::endl;
        }
    }
    std::cout << "==================================================================================\n"
              << "--------------------------------End of contents-----------------------------------\n"
              << "==================================================================================" << std::endl;
    return 0;
}


