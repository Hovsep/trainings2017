#include <iostream>
#include <climits>
#include <unistd.h>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert integers quantity: ";
    }
    int integersQuantity;
    std::cin >> integersQuantity;
    if (integersQuantity < 0) {
        std::cerr << "Error 1: wrong value. Must be positive.\n";
        return 1;
    }
    int minimum = INT_MAX;
    for (int integerCount = 1; integerCount <= integersQuantity; ++integerCount) {
        int integer;
        if (::isatty(STDIN_FILENO)) {
            std::cout << "Insert number " << integerCount << ": ";
        }
        std::cin >> integer;
        if (minimum > integer) {
            minimum = integer;
        }
    }
    if (integersQuantity != 0) {
        std::cout << "Minimum is: " << minimum << std::endl;
    } else {
        std::cout << "Nothing to print!!!." << std::endl;
    }
    return 0;
}
