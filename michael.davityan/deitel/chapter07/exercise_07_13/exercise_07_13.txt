Question!!!.
------------------------------------------------------------------------------------------------------
Write single statements that perform the following one-dimensional array operations:
a. Initialize the 10 elements of integer array counts to zero.
b. Add 1 to each of the 15 elements of integer array bonus.
c. Read 12 values for double array monthly Temperatures from the keyboard.
d. Print the 5 values of integer array bestScores in column format.
======================================================================================================

Answer!!!.
------------------------------------------------------------------------------------------------------
a. For example: 
   const int SIZE = 10;

   first  method: int counts[SIZE] = {0};

                  int counts[SIZE];
   second method: for (int row = 0; row < SIZE; ++row) {
                      counts[row] = 0;    
                  }
------------------------------------------------------------------------------------------------------
b.
  for (int row = 0; row < 15; ++row) {
      ++bonus[row];    
  }
------------------------------------------------------------------------------------------------------
c.
  const int SIZE = 12;
  double monthlyTemperatures[SIZE];
  for (int row = 0; row < SIZE; ++row) {
      std::cin >> monthlyTemperatures[row];
  }
------------------------------------------------------------------------------------------------------
d.
  for (int row = 0; row < 5; ++row) {
      std::cout << bestScores[row] << " ";    
  }
  std::cout << std::endl;
======================================================================================================


