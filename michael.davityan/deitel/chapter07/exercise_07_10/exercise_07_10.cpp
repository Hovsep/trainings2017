#include <iostream>
#include <unistd.h>

int
main() 
{
    int workersQuantity;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert workers quantity: ";
    }
    std::cin >> workersQuantity;
    if (workersQuantity <= 0) {
        std::cerr << "Error 1: Quantity must be more than zero.\n";
        return 1;
    } 
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert the gross sale for a week for each of worker.\n";
    }
    const int ARRAY_SIZE = 9;
    int salaryFrequency[ARRAY_SIZE] = {0};
    for (int worker = 1; worker <= workersQuantity; ++worker) {
        int grossSale;
        if (::isatty(STDIN_FILENO)) {
            std::cout << "worker" << worker << ": ";
        }
        std::cin >> grossSale;
        if (grossSale < 0) {
            std::cerr << "Error 2: must be non-negative value.\n";
            return 2;
        }
        const int salary = 200 + grossSale * 9 / 100;  
        if (salary > 1000) {
            ++salaryFrequency[8];
            continue;
        }
        ++salaryFrequency[salary / 100 - 2];
    }

    std::cout << "salary frequency\tworkers quantity\n"; 
    for (int index = 0; index < ARRAY_SIZE; ++index) {
        if (index != 8) {
            std::cout << "$" << (index + 2) * 100 << "-$" << (index + 2) * 100 + 99
                      << "\t\t" << salaryFrequency[index] << std::endl;
            continue;
        }
        std::cout << "$" << (index + 2) * 100 << "-...\t\t" 
                  << salaryFrequency[index] << std::endl;
    }

    return 0;
}
