#include <iostream>
#include <unistd.h> 
#include <iomanip>
#include <cassert>

int
main()
{
    /// (1) This part of program takes from user a price per product and writes 
    /// into array pricePerProduct[].
    const size_t PRODUCT_COUNT = 5;
    const size_t SELLERS_COUNT = 4;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert price per product.\n";
    }
    size_t pricePerProduct[PRODUCT_COUNT];
    for (size_t index = 0; index < PRODUCT_COUNT; ++index) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "product " << index + 1 << ": ";
        }
        std::cin >> pricePerProduct[index];
    }
    /// End of part (1).
    /// -----------------------------------------------------------------------------------------------
    /// (2) This part of program takes the quantity of selled product per seller from user,
    /// multiplies it to product price, and writes the result into array sales[][].
    if (::isatty(STDIN_FILENO)) {
        std::cout << "insert the quantity of selled products\n" 
                  << "per seller during the month.\n";
    }
    size_t sales[PRODUCT_COUNT][SELLERS_COUNT];
    for (size_t seller = 0; seller < SELLERS_COUNT; ++seller) {
        for (size_t product = 0; product < PRODUCT_COUNT; ++product) {
            size_t quantityPerProduct; 
            if (::isatty(STDIN_FILENO)) {
                std::cout << "Seller " << seller + 1 
                          << ", product " << product + 1 << ": ";
            }
            std::cin >> quantityPerProduct;
            sales[product][seller] = quantityPerProduct * pricePerProduct[product]; 
        }
        if (::isatty(STDIN_FILENO)) {
            std::cout << "------------------------------------------------\n";
        }
    }
    /// End of part (2).
    /// -----------------------------------------------------------------------------------------------
    /// (3) This part of program sums up all received data by products(line elements sum) in 
    /// array totalsForEachProduct[], and by sellers(column elements sum) in totalsForEachSeller
    /// array. Also this part of program define the maximum element of totalsForEachSeller array for
    /// setw function.
    size_t totalsForEachSeller[SELLERS_COUNT] = {0};
    size_t totalsForEachProduct[PRODUCT_COUNT] = {0};
    int sumOfTotalsForProducts = 0;
    for (size_t product = 0; product < PRODUCT_COUNT; ++product) {
        for (size_t seller = 0; seller < SELLERS_COUNT; ++seller) {
            totalsForEachProduct[product] += sales[product][seller];            
            totalsForEachSeller[seller] += sales[product][seller];
        }
        sumOfTotalsForProducts += totalsForEachProduct[product];
    }
    int sumOfTotalsForSellers = 0;
    for (size_t seller = 0; seller < SELLERS_COUNT; ++seller) {
        sumOfTotalsForSellers += totalsForEachSeller[seller];
    }    
    assert(sumOfTotalsForSellers == sumOfTotalsForProducts);
    int maximum = sumOfTotalsForSellers;    
    int maximumDigitsCount = 0;
    do {
        maximum /= 10;
        ++maximumDigitsCount;
    } while (maximum != 0);
    maximumDigitsCount += 6;
    /// End of part (3).
    /// -----------------------------------------------------------------------------------------------
    /// (4) And this part of program finaly print all results in tabulated form.
    std::cout << "-----------------------------------------------------------------------------------"               << "--------------------\n";
    std::cout << "           |";
    for (size_t seller = 0; seller < SELLERS_COUNT; ++seller) {
        std::cout << std::setw(maximumDigitsCount) << " Seller" << seller + 1 << "|"; 
    }
    std::cout << std::setw(maximumDigitsCount + 2) << "  Total|";
    std::cout << "\n";
    std::cout << "-----------------------------------------------------------------------------------"               << "--------------------\n";
    for (size_t product = 0; product < PRODUCT_COUNT; ++product) {
        std::cout << "Product " << product + 1 << "  |";
        for (size_t seller = 0; seller < SELLERS_COUNT; ++seller) {
            std::cout << std::setw(maximumDigitsCount) << sales[product][seller] << "$|";
        } 
        std::cout << std::setw(maximumDigitsCount) << totalsForEachProduct[product] 
                  << "$|" << "\n";
    }
    std::cout << "-----------------------------------------------------------------------------------"               << "--------------------\n";
    std::cout << "Total      |";
    for (size_t seller = 0; seller < SELLERS_COUNT; ++seller) {
        std::cout << std::setw(maximumDigitsCount) << totalsForEachSeller[seller] << "$|";
    }
    std::cout << std::setw(maximumDigitsCount) << sumOfTotalsForSellers << "$|";
    std::cout << "\n";
    std::cout << "-----------------------------------------------------------------------------------"               << "--------------------" << std::endl;
    return 0;
    /// End of part (4), and all program!!!!.
}
