#include <iostream>
#include <iomanip>

int
main()
{
    int sideValue; /// variable for rectangle side value.

    std::cin >> sideValue; /// initialize rectangle side by value.

    if (sideValue < 1) {
        std::cout << "Error 1: wrong side." << std::endl;
        return 1;
    }
    if (sideValue > 20) {
        std::cout << "Error 1: wrong side." << std::endl;
        return 1;
    }

    int line = 1;

    while (line <= sideValue) {
        int column = 1;
        while (column <= sideValue) {
            if (1 == line) {
                std::cout << "*";
            } else if (sideValue == line) {
                std::cout << "*";
            } else if (1 == column) {
                std::cout << "*";
            } else if (sideValue == column) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }

            ++column;    
        }

        ++line;
        std::cout << std::endl;
    }
    
    return 0;
}
