#include <iostream>
#include <cstdlib>
#include <cassert>
#include <ctime>
#include <unistd.h>

void printMessage(const bool answerStatus);
void printPassMessage(const int messageType);
void printNotPassMessage(const int messageType);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::srand(std::time(0));
    } 
    int number1 = 0;
    int number2 = 0;
    bool answerStatus = true;
    while (true) {
        if (::isatty(STDIN_FILENO)) {
            std::cout << "How much is  "; 
        }
        if (answerStatus) {
            number1 = 1 + std::rand() % 10;
            number2 = 1 + std::rand() % 10;
        }
        if (::isatty(STDIN_FILENO)) {
            std::cout << number1 << " * " << number2 << "?: "; 
        }
        int answer;
        std::cin >> answer;
        answerStatus = (number1 * number2 == answer);
        if (-1 == answer) {
            break;
        }
        printMessage(answerStatus); 
    }
    return 0;
}

void 
printMessage(const bool answerStatus) 
{
    const int messageType = 1 + std::rand() % 4;
    if (answerStatus) {
        printPassMessage(messageType);
    } else {
        printNotPassMessage(messageType);
    }
}

void 
printPassMessage(const int messageType)
{
    assert((messageType >= 1) && (messageType <= 4));
    switch (messageType) {
    case 1: std::cout << "Very good!\n"            ; break;
    case 2: std::cout << "Excellent!\n"            ; break;
    case 3: std::cout << "Nice work!\n"            ; break;
    case 4: std::cout << "Keep up the good work!\n"; break;     
    }
}

void printNotPassMessage(const int messageType)
{
    assert((messageType >= 1) && (messageType <= 4));
    switch (messageType) {
    case 1: std::cout << "No. please try again.: "; break;
    case 2: std::cout << "Wrong. Try once more.: "; break;
    case 3: std::cout << "Don't give up!: "       ; break;
    case 4: std::cout << "No. Keep trying.: "     ; break;     
    }
}


