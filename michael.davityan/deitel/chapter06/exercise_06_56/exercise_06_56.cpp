#include <iostream>
#include <unistd.h>

int tripleCallByValue(int number);
void tripleByReference(int &number);

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "This program shows the difference betwen functions\n"
                  << "call by value, and call by reference.\n"
                  << "Now in this program we have two functions\n"
                  << "tripleCallByValue and tripleByReference,\n"
                  << "that do that job!!!!\n"
                  << "Insert a whole number: ";
    }
    int number; 
    std::cin >> number;
    std::cout << "number value before call by value: " << number << "\n";
    std::cout << "tripleCallByValue function returned value: " 
              << tripleCallByValue(number) << "\n";
    std::cout << "number value after  call by value: " << number << "\n";
    
    std::cout << "number value before call by reference: " << number << "\n";
    tripleByReference(number);
    std::cout << "number value after  call by reference: " << number << std::endl;

    return 0;
}

int 
tripleCallByValue(int number)
{
    number *= 3;
    return number;
}

void 
tripleByReference(int &number)
{
    number *= 3;
}
