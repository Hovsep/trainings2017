#include <iostream>
#include <unistd.h>
#include <cassert>

inline double 
circleArea(const double radius) 
{
    assert(radius >= 0);
    return 3.14159 * radius * radius;
}

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "This program takes from user circle radius, calculate\n"
                  << "and return circle area\n"
                  << "Insert circle radius: ";
    }
    double radius;
    std::cin >> radius;
    if (radius < 0) {
        std::cerr << "Error 1: must be positive value.\n";
        return 1; 
    }
    std::cout << "Area of the circle is " << circleArea(radius) << std::endl; 

    return 0;
}
