$(document).ready(function () {
    "use strict";
    var obj = {
        className: "open menu menu"
    };
    function removeClass(obj, cls) {
        var arr = obj.className.split(" "), newArr = [], i = 0;
        for (i = 0; i < arr.length; ++i) {
            if (arr[i] === cls) {
                continue;
            }
            newArr.push(arr[i]);
        }
        obj.className = newArr.join(" ");
        console.log(obj);
    }
    removeClass(obj, "blabla");
});			
