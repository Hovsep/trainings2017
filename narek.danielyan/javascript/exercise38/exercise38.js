$(document).ready(function () {
    "use strict";
    var vasya = { name: 'Вася', age: 65}, masha = { name: 'Маша', age: 32 }, vovochka = { name: 'Вовочка', age: 46 }, people = [vasya, masha, vovochka], i = 0, j = 0, temperory = 0;
    for (i =  0; i < people.length; ++i) {
        for (j = i + 1; j < people.length; ++j) {
            if (!people[i].age && !people[j].age) {
                throw new Error("One of the objects doesn't have key age");
            }
            if (people[i].age > people[j].age) {
                temperory = people[i];
                people[i] = people[j];
                people[j] = temperory;
            }
        }
    }
    console.log(people[1]);
    for (i = 0; i < people.length; ++i) {
        console.log(people[i].age);
    }
});
