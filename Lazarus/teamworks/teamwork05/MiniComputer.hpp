#include <string>

class MiniComputer
{
public:
    MiniComputer(std::string name);
    int run();
    std::string getComputerName();
    void setComputerName(std::string name);
private:
    void showMainMenu();
    int getMainCommand();
    int executeCommand(int command);
    bool isRegisterError(int registerNumber);
    int executeLoadCommand();
    int loadRegister(std::string registerName);
    int executeStoreCommand();
    int getRegisterValue(int registerNumber);
    std::string getRegisterName(int registerNumber);
    void setRegisterValue(int registerNumber, int registerValue);
    int executeAddCommand();
    int executePrintCommand();
private:
    std::string computerName_;
    int registerA_;
    int registerB_;
    int registerC_;
    int registerD_;
};

