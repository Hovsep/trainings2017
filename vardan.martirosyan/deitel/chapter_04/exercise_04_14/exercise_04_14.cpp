#include <iostream>
#include <iomanip>

int
main()
{
    int accountNumber = 0;

    while (true) {
        std::cout << "\nEnter account number (-1 to end): ";
        std::cin  >> accountNumber;
        if (-1 == accountNumber) {
            return 0;
        }
        if (accountNumber < 0) {
            std::cerr << "Error 1: Account number cannot be negative." << std::endl;
            return 1;
        }

        double balance;
        std::cout << "Enter balance: ";
        std::cin  >> balance;
        if (balance < 0) {
            std::cerr << "Error 2: The beginning balance cannot be negative." << std::endl;
            return 2;
        }

        double totalCharges;
        std::cout << "Enter total charges: ";
        std::cin  >> totalCharges;
        if (totalCharges < 0) {
            std::cerr << "Error 3: Total Charges cannot be negative" << std::endl;
            return 3;
        }

        double totalCredits;
        std::cout << "Enter total credits: ";
        std::cin  >> totalCredits;
        if (totalCredits < 0) {
            std::cerr << "Error 4: Total Credits cannot be negative" << std::endl;
            return 4;
        }

        double creditLimit;
        std::cout << "Enter credit limit: ";
        std::cin  >> creditLimit;
        if (creditLimit < 0) {
            std::cerr << "Error 5: Credit Limit cannot be negative." << std::endl;
            return 5;
        }

        double newBalance = balance + totalCharges - totalCredits;
        std::cout << "New balance is: " << std::setprecision(2) << std::fixed << newBalance << std::endl;

        if (newBalance > creditLimit) {
            std::cout << "Account: "<< std::setprecision(2) << std::fixed << accountNumber << std::endl;

            std::cout << "Credit limit: " << std::setprecision(2) << std::fixed << creditLimit << std::endl;

            std::cout << "Balance: " << std::setprecision(2) << std::fixed << balance << std::endl;

            std::cout << "Credit Limit Exceeded." << std::endl;
        }
    }
    return 0;
}

