#include <iostream>

int
main()
{
    std::cout << "Number\tFactorial of number\n";

    int factorial = 1;
    for (int number = 1; number <= 5; ++number) {
        factorial *= number;
        std::cout << number << "!\t" << factorial << std::endl;
    }
    return 0;
}

