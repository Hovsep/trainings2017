#include <iostream>
#include <cmath>

int
main()
{   
    int size;
    std::cout << "Enter size.";
    std::cin  >> size;
    if(0 == (size & 1) || size > 19 || size < 1) {
        std::cerr << "Error 1: Invalid size." << std::endl;
        return 1;
    }
    int evenSize = ((size - 1) / 2);
    for (int yAxis= -evenSize; yAxis <= evenSize; ++yAxis) {
        for (int xAxis = -evenSize; xAxis <= evenSize; ++xAxis) {
            std::cout << (std::abs(xAxis) + std::abs(yAxis) <= evenSize ? "*" : " "); 
        }
        std::cout << std::endl;
    }
    return 0;
}

