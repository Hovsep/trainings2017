#include <iostream>
#include <iomanip>

int 
main()
{
    float pi = 0;
    int evenDenominator = 1, oddDenominator = 1, longPi = 314159;
    int simbol = 1;

    for (int intAccuracy = 1000, floatAccuracy = 100; intAccuracy > 0; intAccuracy /= 10, floatAccuracy *= 10) {
        int intPi = longPi / intAccuracy;
        float delta = 0;
        do {
            pi += (4.0 / evenDenominator * simbol); 
            simbol *= -1;
            evenDenominator += 2;
            ++oddDenominator;
            delta = pi * floatAccuracy - intPi;
        } while (delta >= 0.05 || delta < 0);
        std::cout << "Pi is " << std::setprecision(5) << std::fixed  << pi << std::setw(20)<< "Accuracy is " << oddDenominator << std::endl;
    }
    return 0;
}
